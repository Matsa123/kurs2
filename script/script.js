window.onload = function () {

    const xhr = new XMLHttpRequest();
    const date = document.querySelector('.date');
    const btn = document.querySelector('#bTn');

    xhr.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");

    btn.addEventListener('click', () => {
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
            
                let data = JSON.parse(xhr.responseText);

                date.innerHTML += data[0].exchangedate;

                let table = document.querySelector(".tab");

                data.forEach(i => {
                    if (i.rate > 25) {
                        let tr = document.createElement("tr");
                        let td1 = document.createElement("td");
                        let td2 = document.createElement("td");
                        let td3 = document.createElement("td");
                        table.appendChild(tr);
                        tr.appendChild(td1);
                        tr.appendChild(td2);
                        tr.appendChild(td3);
                        td1.innerHTML = i.txt;
                        td2.innerHTML = i.cc;
                        td3.innerHTML = i.rate.toFixed(2) + "грн";
                    }
                });

            } else {
                if (xhr.status == 400) {
                    alert("Клиентская ошибка! Код 400")
                }
                if (xhr.status == 500) {
                    alert("Серверная ошибка! Код 500")
                }
            }
        }
        xhr.send();
        btn.remove();
    });


}
